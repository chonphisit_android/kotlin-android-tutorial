package buu.chonphisit.kotlinandroidtutorial1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.Toast

class AutoCompleteTextViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auto_complete_text_view)
        val autoTextView = findViewById<AutoCompleteTextView>(R.id.autoTextView)
        val languages = resources.getStringArray(R.array.Languages)
        val adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, languages)
        autoTextView.setAdapter(adapter)

        val button = findViewById<Button>(R.id.btnSubmit)
        button.setOnClickListener {
            val enterText = getString(R.string.submitted_lang) + autoTextView.getText().toString()
            Toast.makeText(this@AutoCompleteTextViewActivity, enterText, Toast.LENGTH_LONG).show()
        }
    }
}